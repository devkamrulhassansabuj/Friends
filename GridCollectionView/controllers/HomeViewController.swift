//
//  HomeViewController.swift
//  GridCollectionView
//
//  Created by Kamrul Hassan Sabuj
//

import UIKit

class HomeViewController: UIViewController {
    let numberOfColumns: CGFloat = 3
    var items: Array<Item> = Array()
    private var viewModels = [ItemCollectionViewModel]()
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        fetchProfile()
    }
    
    //MARK: - View Initializations
    func initViews(){
        initNavigationBar()
        collectionView.delegate = self
        collectionView.dataSource = self
        
        self.collectionView.register(UINib(nibName: "ItemCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "myCell")
        
        if let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            let screenSize: CGRect = UIScreen.main.bounds
            let cellWidth = screenSize.width / numberOfColumns - 15
            flowLayout.itemSize = CGSize(width: cellWidth, height: cellWidth)
        }
        
    }
    //MARK: - Fetch Profile Data From API
    private func fetchProfile() {
        APICaller.shared.getProfile { [weak self] data in
            switch data{
            case .success(let itemsList):
                self?.items = itemsList
                self?.viewModels = self!.items.compactMap({
                    ItemCollectionViewModel(title: "\($0.name.title) \($0.name.first) \($0.name.last)",
                                            country: $0.location.country,
                                            imageURL: URL(string: $0.picture.large)
                    )
                })
                DispatchQueue.main.async {
                    self?.collectionView.reloadData()
                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    //MARK: - Navigation Bar
    func initNavigationBar(){
        title = "Home"
    }
}
//MARK: - CollectionView Delegate and Data Source
extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myCell", for: indexPath) as! ItemCollectionViewCell
        cell.configure(with: viewModels[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = ScrollViewController(nibName: "ScrollViewController", bundle: nil)
        vc.items = items[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: false)
    }
}
