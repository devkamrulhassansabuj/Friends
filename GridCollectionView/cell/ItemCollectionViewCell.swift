//
//  ItemCollectionViewCell.swift
//  GridCollectionView
//
//  Created by Kamrul Hassan Sabuj

import UIKit
class ItemCollectionViewCell: UICollectionViewCell {
    //IBOutlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageTitle: UILabel!
    @IBOutlet weak var imageCountry: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        
        imageView.image = nil
        imageTitle.text = nil
        imageCountry.text = nil
    }
    //MARK: - Cell Configaration
    func configure(with viewModel: ItemCollectionViewModel) {
        imageTitle.text = viewModel.title
        imageCountry.text = viewModel.country
        
        if let data = viewModel.imageData {
            imageView.image = UIImage(data: data)
        }
        
        else if let url = viewModel.imageURL {
            URLSession.shared.dataTask(with: url) { [weak self] data, _, error in
                guard let data = data, error == nil else { return }
                
                viewModel.imageData = data
                
                DispatchQueue.main.async {
                    self?.imageView.image = UIImage(data: data)
                }
            }.resume()
        }
    }
    
}

