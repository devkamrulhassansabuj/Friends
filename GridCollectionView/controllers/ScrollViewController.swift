//
//  ScrollViewController.swift
//  GridCollectionView
//

//
import UIKit
import MessageUI

class ScrollViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var country: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var fullName: UITextField!
    
    var imageData: Data? = nil
    var items: Item!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initNavigationBar()
    }
    //Email Tapped
    @IBAction func emailPressed(_ sender: UIButton) {
        openMail()
    }
    //MARK: - Initial Setup
    private func initNavigationBar(){
        title = "Detailes"
        self.fullName.text = "\(items.name.title) \(items.name.first) \(items.name.last)"
        self.fullName.isUserInteractionEnabled = false
        self.email.isUserInteractionEnabled = false
        self.address.isUserInteractionEnabled = false
        
        self.country.isUserInteractionEnabled = false
        self.phoneNumber.isUserInteractionEnabled = false
        
        self.email.text = items.email
        self.phoneNumber.text = items.phone
        self.country.text = items.location.country
        self.address.text = "\(items.location.state) , \(items.location.city)"
        
        if let data = imageData {
            imageView.image = UIImage(data: data)
        }
        
        else if let url = URL(string: items.picture.large) {
            URLSession.shared.dataTask(with: url) { [weak self] data, _, error in
                guard let data = data, error == nil else { return }
                
                self?.imageData = data
                
                DispatchQueue.main.async {
                    self?.imageView.image = UIImage(data: data)
                }
            }.resume()
        }
        let back = UIImage(named: "ic_back")
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: back, style: .plain, target: self, action: #selector(onBackPressed))
    }
    
    @objc func onBackPressed(){
        navigationController?.popViewController(animated: false)
    }
    //MARK: - Open Mail App
    private func openMail(){
        if let appURL = URL(string: "mailto:\(items.email)"){
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(appURL)
            }
        }
    }
}
