//
//  Item.swift
//  GridCollectionView
//
//  Created by Kamrul Hassan Sabuj
//

import Foundation

struct APIResponse: Codable {
    let results: [Item]
}
// MARK: - Result
struct Item: Codable {
    let gender: String
    let email: String
    let phone: String
    let name: Name
    let picture: Picture
    let location: Location
}
// MARK: - Name
struct Name: Codable {
    let title, first, last: String
}
struct Picture: Codable {
    let large, medium, thumbnail: String
}
// MARK: - Location
struct Location: Codable {
    let city, state, country: String
}
// MARK: - Item Modle 
class ItemCollectionViewModel {
    let title: String
    let country: String
    let imageURL: URL?
    var imageData: Data? = nil
    
    init (
        title: String,
        country: String,
        imageURL: URL?
    ) {
        self.title = title
        self.country = country
        self.imageURL = imageURL
    }
}
