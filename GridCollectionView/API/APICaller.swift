//
//  APICaller.swift
//  GridCollectionView
//
//  Created by Kamrul Hassan Sabuj on 21/5/21.
//

import Foundation
//MARK: - API Calling Singleton
class APICaller {
    static let shared = APICaller()
    struct Constants {
        static let topURL = "https://randomuser.me/api/?results=12"
    }
    private init() {
    }
    public func getProfile(completion: @escaping (Result<[Item],Error>) -> Void) {        
        let urlString = Constants.topURL
        guard let url = URL(string: urlString) else {return}
        
        let task = URLSession.shared.dataTask(with: url) { data, _, error in
            if let error = error {
                completion(.failure(error))
            }
            
            else if let data = data {
                do {
                    let result = try JSONDecoder().decode(APIResponse.self, from: data)
                    completion(.success(result.results))
                }
                catch {
                    completion(.failure(error))
                }
            }
        }
        
        task.resume()
    }
    
    
}
